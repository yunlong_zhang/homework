/**
 * Created by 云龙你好 on 2016/2/27.
 */
$(document).ready(function(){
    function inherit(p)
    {
        if(p==null)throw TypeError();
        if(Object.create)
            return Object.create(p);
        var t=typeof p;
        if(t!=="object" && t!=="function")throw  TypeError();
        function f(){};
        f.prototype=p;
        return new f();
    }
    function Point(a,b){
        var P=inherit(Point.distance);
        P.a=a;
        P.b=b;
        return P;
    }
    Point.distance={
        range:function(c,d){var dis=Math.sqrt((this.a-c)*(this.a-c)+(this.b-d)*(this.b-d));console.log(dis)}
    };
    var P=Point(0,0);
    P.range(1,1);
});