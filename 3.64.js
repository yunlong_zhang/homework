/**
 * Created by 云龙你好 on 2016/2/27.
 */
$(document).ready(function(){
    function inherit(p)
    {
        if(p==null)throw TypeError();
        if(Object.create)
            return Object.create(p);
        var t=typeof p;
        if(t!=="object" && t!=="function")throw  TypeError();
        function f(){};
        f.prototype=p;
        return new f();
    }
    function Imaginary(a,b){
        var I=inherit(Imaginary.computer);
        I.a=a;
        I.b=b;
        return I;
    }
    Imaginary.computer={
        sum:function(){var sum=this.a+this.b;console.log(this.a+"加"+this.b+"是："+sum)},
        sub:function(){var sub=this.a-this.b;console.log(this.a+"减"+this.b+"是："+sub)},
        mul:function(){var mul=this.a*this.b;console.log(this.a+"乘"+this.b+"是："+mul)},
        div:function(){var div=this.a/this.b;console.log(this.a+"除以"+this.b+"是："+div)}
    };
    var I=Imaginary(5,6);
    I.sum();
    I.sub();
    I.mul();
    I.div();
});