/**
 * Created by 云龙你好 on 2016/2/27.
 */
$(document).ready(function(){
    function CDATAINFO(year,month,day)
    {
        var CD=inherit(CDATAINFO.print);
        CD.year=year;
        CD.month=month;
        CD.day=day;
        return CD;

    }
    CDATAINFO.print={
        one: function(){console.log("2000年1月1日");},
        two: function(y,m,d){console.log(y+"年"+m+"月"+d+"日")},
        three: function(){
            var date=new Date();
            var year=date.getFullYear();
            var month=date.getMonth();
            var day=date.getDate();
            var hours=date.getHours();
            var minutes=date.getMinutes();
            var seconds=date.getSeconds();
            console.log(year+"年"+month+"月"+day+"日"+hours+"时"+minutes+"分"+seconds+"秒");
        }

    };

    function CDataInfo(year,month,day) {
        this.year=year;
        this.month=month;
        this.day=day;
    }
    CDataInfo.prototype={
        one: function(){console.log("2000年1月1日");},
        two: function(y,m,d){console.log(y+"年"+m+"月"+d+"日")}
    };
    var CD=CDATAINFO();
    CD.one();
    CD.two(2016,2,27);
    CD.three();
    /*创建对象的函数inherit*/
    function inherit(p)
    {
        if(p==null)throw TypeError();
        if(Object.create)
            return Object.create(p);
        var t=typeof p;
        if(t!=="object" && t!=="function")throw  TypeError();
        function f(){};
        f.prototype=p;
        return new f();
    }
});
